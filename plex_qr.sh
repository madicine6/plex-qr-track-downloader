#!/bin/bash

user=''
local_addr='localhost:32400'
remote_addr=''
token=''
xml='/tmp/qr.xml'

wsta "ws://$local_addr/:/websockets/notifications?X-Plex-Token=$token" | while read -r event; do
	if [[ $event != '?' ]]; then
		notif_type="$(echo "$event" | jq ".NotificationContainer.type")"
		if [[ $notif_type == '"playing"' ]]; then
			prev_key="$key"
			key="$(echo "$event" | jq ".NotificationContainer.PlaySessionStateNotification[0].key" | tr -d '"')"
			if [[ $key != $prev_key ]]; then
				curl -s "http://$local_addr$key?X-Plex-Token=$token" > "$xml"
				if [[ ! -z $(xmllint "$xml" --xpath '//MediaContainer/Track') ]]; then
					file=$(xmllint "$xml" --xpath 'string(//MediaContainer/Track/Media/Part/@key)')
					link="http://$remote_addr$file?download=1&X-Plex-Token=$token"
					title="$(xmllint "$xml" --xpath 'string(//MediaContainer/Track/@title)')"
					artist="$(xmllint "$xml" --xpath 'string(//MediaContainer/Track/@grandparentTitle)')"
					album="$(xmllint "$xml" --xpath 'string(//MediaContainer/Track/@parentTitle)')"
					echo "$artist - $title [$album]"
					echo "$link"
					qrencode "$link" -t UTF8 -
				fi
			fi
		fi
	fi
done