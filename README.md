# Plex QR Track Downloader

Script that generates QR codes with download link of currently playing track.

# Dependencies

wsta, jq, xmllint, qrencode

# Usage

1. Edit script to put your plex username, token and server address in it.
2. Run it.
3. Enjoy!